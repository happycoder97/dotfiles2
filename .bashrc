source ~/.aliases

#  UTILITY FUNCTIONS -----------------------------------------------------------

load_env() {
    if [[ -f "load_env.bash" ]]; then
        source "load_env.bash";
    elif [[ -f "../load_env.bash" ]]; then
        source "../load_env.bash";
    elif [[ -f "../../load_env.bash" ]]; then
        source "../../load_env.bash";
    else
        echo "load_env.bash not found in '.' , '..' or '../..'";
    fi;
}

#  BASH SETTINGS ---------------------------------------------------------------

if [ -z $PROMPT_HIDE_DIR ]; then
    PROMPT_HIDE_DIR=0;
fi

# Prompt
prompt() {
    lastret=$?
    function print_gray() {
        echo -ne "\[\e[1;38;5;104m\]$1\[\e[0m\]"
    }

    right="$lastret"
    if [ $PROMPT_HIDE_DIR -eq 0 ]; then
        left="\w)"
    else
        left=""
    fi;
    main="$PS_MODE$"
    pad="                    "
    pad="$pad$pad$pad$pad"
    pad="$pad$pad$pad$pad"
    pad="$pad$pad$pad$pad"
    pad="$pad$pad$pad$pad"
    len="$(($(tput cols)-${#right}-2))"

    PS1=$(printf "\n%*.*s %s\r%s\n\[\e[1;90m\]%s\[\e[0m\] " 0 $len "$pad" "$(print_gray $right)" "$(print_gray $left)" "$main")
}
PROMPT_COMMAND=prompt

prompt_hide_dir() {
    export PROMPT_HIDE_DIR=1
}

#  FASD ------------------------------------------------------------------------
# Note: This needs to be loaded after we set prompt command
if [ -n "$(command -v fasd)" ]; then
    eval "$(fasd --init auto)"
    alias v="fasd -e nvim"
fi

# History
export HISTSIZE=1000000
export HISTFILESIZE=1000000000
export HISTCONTROL=ignoreboth  # ignorespace + ignoredups

# append instead of overwrite
shopt -s histappend
# append history whenever prompt is displayed
PROMPT_COMMAND="$PROMPT_COMMAND; history -a"
# history is now shared between shells. yay! :)

#  PLUGINS ---------------------------------------------------------------------

# FZF
if [ -f "/usr/share/fzf/completion.bash" ]; then
    source "/usr/share/fzf/completion.bash";
fi
if [ -f "/usr/share/fzf/key-bindings.bash" ]; then
    source "/usr/share/fzf/key-bindings.bash";
fi

#  LOAD HOST SPECIFIC BASHRC ---------------------------------------------------

HOST=$(hostname)
if [[ -f ~/.bashrc-$HOST ]]; then
   source ~/.bashrc-$HOST
fi
