# vi:foldmethod=marker
set -g default-terminal screen-256color
set -ga terminal-overrides ",*256col*:Tc"
set -g default-command "exec $SHELL"
set-window-option -g mode-keys vi

set-option -g status-position top
# set -g status-left "#S (#(pwd)) "
set -g status-left "  #[fg=red]#S  "
set -g status-right "#T  #[fg=green]#(pwd)  #[fg=red]%c"
set-option -g status-bg "#101419"
set-option -g status-fg "#d2a6ff"

set -g status-justify left
set -g status-left-length 85
set -g status-right-length 200

# --- ./home.include/000_prefix.tmux ---
unbind C-b
set -g prefix C-x
bind-key C-x send-prefix

set-option -g mouse on
set -g base-index 1
set -g pane-base-index 1

# --- ./home.include/001_plugins.tmux ---
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# --- ./home.include/plugin_sidebar.tmux ---
set -g @plugin 'tmux-plugins/tmux-sidebar'

set -g @sidebar-tree-position 'top'
set -g @sidebar-tree 'e'
set -g @sidebar-tree-command 'tree -C -L 3'
set -g @sidebar-tree-pager 'less -R'

set -g @plugin 'nhdaly/tmux-better-mouse-mode'
set -g @emulate-scroll-for-no-mouse-alternate-buffer "on"

# {{{ Pane navigation

	bind-key -n 'M-h' select-pane -L
	bind-key -n 'M-j' select-pane -D
	bind-key -n 'M-k' select-pane -U
	bind-key -n 'M-l' select-pane -R
	bind-key -n 'M-\' select-pane -l
	bind-key -n 'M-;' display-panes
          
	bind-key -n 'C-M-h' resize-pane -L 5
	bind-key -n 'C-M-j' resize-pane -D 5
	bind-key -n 'C-M-k' resize-pane -U 5
	bind-key -n 'C-M-l' resize-pane -R 5

	bind-key -T copy-mode-vi 'M-h' select-pane -L
	bind-key -T copy-mode-vi 'M-j' select-pane -D
	bind-key -T copy-mode-vi 'M-k' select-pane -U
	bind-key -T copy-mode-vi 'M-l' select-pane -R
	bind-key -T copy-mode-vi 'M-\' select-pane -l

	bind-key -n 'M-H' split-pane -hb
	bind-key -n 'M-J' split-pane -v
	bind-key -n 'M-K' split-pane -vb
	bind-key -n 'M-L' split-pane -h

# }}}

bind-key -n M-c attach-session -c "#{pane_current_path}"
bind-key -n M-[ copy-mode

# Run GNU Make
bind-key / "new-window \"make; echo Program exited..; read -n 1\""

bind-key r source-file ~/.tmux.conf \; display-message "~/.tmux.conf reloaded"
bind-key M split-window -h "nvim ~/.tmux.conf"

# --- ./home.include/zzz_plugins_init.tmux ---
run '~/.tmux/plugins/tpm/tpm'
