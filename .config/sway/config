
# --- ./home.include/000_modifier_key.i3 ---

set $mod Mod4

# --- ./home.include/001_font_config.i3 ---
font pango:Roboto Regular 10

# --- ./home.include/002_basic_shortcuts.i3 ---

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+q kill

# change focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right


# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right


# split in vertical orientation
bindsym $mod+v split v

# split in horizontal orientation
bindsym $mod+Shift+v split h

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+comma layout stacking
bindsym $mod+period layout tabbed
bindsym $mod+slash layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
bindsym $mod+Shift+a focus child

# sticky
bindsym $mod+Shift+s sticky toggle

# --- ./home.include/003_workspace.i3 ---

# switch to workspace
bindsym $mod+1 workspace number 1: Web
bindsym $mod+2 workspace number 2: Code
bindsym $mod+3 workspace number 3: Code
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7: Docs
bindsym $mod+8 workspace number 8: Gitter
bindsym $mod+9 workspace number 9: Notes
bindsym $mod+0 workspace number 10: Temp

# move focused container to workspace number and switch to workspace
bindsym $mod+Shift+1 move container to workspace number 1: Web;     workspace number 1: Web
bindsym $mod+Shift+2 move container to workspace number 2: Code;    workspace number 2: Code
bindsym $mod+Shift+3 move container to workspace number 3: Code;    workspace number 3: Code
bindsym $mod+Shift+4 move container to workspace number 4;          workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5;          workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6;          workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7: Docs;    workspace number 7: Docs
bindsym $mod+Shift+8 move container to workspace number 8: Gitter;  workspace number 8: Gitter
bindsym $mod+Shift+9 move container to workspace number 9: Notes;   workspace number 9: Notes
bindsym $mod+Shift+0 move container to workspace number 10:Temp;    workspace number 10: Temp

# move focused container to workspace
bindsym $mod+Ctrl+1 move container to workspace number 1: Web
bindsym $mod+Ctrl+2 move container to workspace number 2: Code
bindsym $mod+Ctrl+3 move container to workspace number 3: Code
bindsym $mod+Ctrl+4 move container to workspace number 4
bindsym $mod+Ctrl+5 move container to workspace number 5
bindsym $mod+Ctrl+6 move container to workspace number 6
bindsym $mod+Ctrl+7 move container to workspace number 7: Docs
bindsym $mod+Ctrl+8 move container to workspace number 8: Gitter
bindsym $mod+Ctrl+9 move container to workspace number 9: Notes
bindsym $mod+Ctrl+0 move container to workspace number 10:Temp

bindsym $mod+z workspace back_and_forth
bindsym $mod+Shift+z move container to workspace back_and_forth; workspace back_and_forth
# workspace_auto_back_and_forth yes

# --- ./home.include/004_reload.i3 ---


# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# --- ./home.include/005_resize_mode.i3 ---

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

# --- ./home.include/006_bar.i3 ---
# bar {
# 	font pango:RobotoMono Nerd Font Medium 10
#     position bottom
#     status_command i3status-rs $HOME/.config/i3/i3status-rust-config.toml
# 	colors {
#         # separator #aaaaaa
#         # background #dddddd
#         # statusline #ff0000
# 		# focused_workspace #3260ff #3260ff #ffffff
#         # active_workspace #333333 #333333 #ffffff
# 		# inactive_workspace #aaaaaa #dddddd #333333
# 		# urgent_workspace #58bb04 #58bb04 #ffffff
# 
# 		separator #666666
# 		background #1e2127
#         statusline #dddddd
# 		focused_workspace #3260ff #3260ff #ffffff
#         active_workspace #282c34 #282c34 #ffffff
# 		inactive_workspace #282c34 #282c34 #888888
# 		urgent_workspace #2f343a #900000 #ffffff
# 
# 		# separator #666666
# 		# background #222222
#         # statusline #dddddd
# 		# focused_workspace #0088CC #0088CC #ffffff
#         # active_workspace #333333 #333333 #ffffff
# 		# inactive_workspace #333333 #333333 #888888
# 		# urgent_workspace #2f343a #900000 #ffffff
#     }
# }

bar {
    swaybar_command waybar
}

# --- Colors

# class                 border  backgr. text    indicator child_border
client.focused          #292e40 #31313a #ffffff #292e40   #ff0000
client.focused_inactive #31313a #31313a #bbbbbb #484e50   #31313a
#client.unfocused       #333333 #22262e #888888 #292d2e   #222222
#client.urgent          #2f343a #900000 #ffffff #900000   #900000
#client.placeholder     #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff

smart_borders on
smart_gaps inverse_outer

# gaps inner 10

bindsym $mod+G gaps inner current set 0;
bindsym $mod+Shift+G gaps inner current set 10;
bindsym $mod+B border toggle;

# --- ./home.include/090_custom_border.i3 ---
# for_window [class=".*"] border normal 1
for_window [class="anbox"] floating enable
for_window [app_id="firefox"] border pixel 1
for_window [title=".*Sharing Indicator"] floating enable
for_window [app_id="Chromium"] border pixel 1
for_window [app_id="Tor Browser"] border pixel 1
for_window [app_id="Blender"] border pixel 1
for_window [app_id="Alacritty"] border pixel 1
for_window [app_id="Lxterminal"] border pixel 1
for_window [app_id="XTerm"] border pixel 1
for_window [app_id="Evince"] border pixel 1
for_window [app_id="File-roller"] border pixel 1
for_window [app_id="pcmanfm"] border pixel 1
for_window [app_id="Telegram"] border pixel 1
for_window [app_id="code-oss"] border pixel 0
for_window [app_id="Code"] border pixel 0
for_window [app_id="Audacious"] border pixel 0
for_window [app_id="Notion"] border pixel 0
for_window [app_id="Jitsi Meet"] border pixel 0 floating enable
for_window [app_id="figma-linux"] border pixel 0
for_window [app_id="discord"] border pixel 0
for_window [title="^Android Emulator.*"] border pixel 0 floating enable
for_window [title="Emulator"] border pixel 0 floating enable
for_window [app_id="Pavucontrol"] floating enable
for_window [app_id="Lxappearance"] floating enable
for_window [app_id="KeePassXC"] floating enable
for_window [instance="spotify"] border pixel 0 

# --- ./home.include/999_autolaunch.i3 ---
exec --no-startup-id lxpolkit
exec --no-startup-id nm-applet --indicator
exec --no-startup-id blueman-applet
# exec --no-startup-id xsetroot -solid "#222222"
exec --no-startup-id syncthingtray
exec --no-startup-id keepassxc
# exec --no-startup-id picom
# exec --no-startup-id redshift -l '11.3214604:75.9340919'
# exec --no-startup-id redshift -O 3000 -m randr:crtc=0
# exec --no-startup-id redshift -O 5000 -m randr:crtc=1
exec --no-startup-id dunst
exec --no-startup-id pulseeffects --gapplication-service


# --- ./home.include/app-shortcuts.i3 ---


set $file_explorer	pcmanfm -n
set $web_browser	firefox
set $web_browser2	chromium
set $terminal alacritty

bindsym $mod+Shift+w exec $web_browser2
bindsym $mod+Return	exec --no-startup-id $terminal
#
# --- ./home.include/multimonitor.i3 ---
bindsym $mod+Ctrl+Left move workspace to output Left
bindsym $mod+Ctrl+Right move workspace to output Right

# --- ./home.include/scratchpad.i3 ---
# Scratchpad
bindsym $mod+Shift+BackSpace move scratchpad
bindsym $mod+backslash scratchpad show

# --- ./home.include/xbacklight.i3 ---

for_window [title=".*\[Float\].*"] floating enable

exec --no-startup-id ghostwriter $HOME/Projects/00.Notes/home/notes.auto.md
for_window [title=".* ghostwriter"] move scratchpad
bindsym $mod+N [title=".* ghostwriter"] scratchpad show

for_window [app_id="scratch-term"] move scratchpad
for_window [app_id="scratch-term"] border pixel 1
bindsym $mod+Semicolon exec swaymsg [app_id="scratch-term"] scratchpad show || $terminal --class scratch-term

# bindsym $mod+Shift+Minus move container to scratchpad
bindsym $mod+Apostrophe [app_id="firefox"] scratchpad show

# exec --no-startup-id $HOME/Projects/97.Temp/timely-vue/launch.sh
for_window [class="timely-vue"] move workspace number 9
for_window [class="timely-vue"] border pixel 1

# exec --no-startup-id notion-app
for_window [class="Notion"] move workspace number 7

exec --no-startup-id $web_browser
# for_window [class="firefox"] move workspace number 1

# exec --no-startup-id ferdi
# for_window [class="Ferdi"] move workspace number 8
for_window [class="Ferdi"] border pixel 1

bindsym $mod+M [app_id="telegramdesktop"] scratchpad show

exec systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

# ------------------


bindsym $mod+w exec $web_browser
bindsym $mod+e exec $FILE_EXPLORER
bindsym $mod+Print exec flameshot gui
bindsym $mod+d exec wofi --show drun
bindsym $mod+shift+d exec wofi --show run
bindsym $mod+grave exec wofi --show emoji
bindsym $mod+c exec wofi --show calc
bindsym $mod+shift+n exec networkmanager_dmenu
bindsym $mod+y exec ytplay
bindsym $mod+u exec code

bindsym XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +3dB
bindsym XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -3dB

bindsym $mod+Escape exec xbacklight -set 0.06
bindsym $mod+F1 exec xbacklight -5
bindsym $mod+F2 exec xbacklight +5

bindsym $mod+F9 exec xrandr --output HDMI1 --mode 2560x1440 --primary; xrandr --output eDP1 --off; xrandr --dpi 93.26
bindsym $mod+F10 exec xrandr --output eDP1 --mode 1920x1080 --primary; xrandr --output HDMI1 --off; xrandr --dpi 157.35

exec systemctl --user import-environment DISPLAY WAYLAND_DISPLAY SWAYSOCK
exec hash dbus-update-activation-environment 2>/dev/null && \
     dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY SWAYSOCK

input type:touchpad {
    tap enabled
    natural_scroll enabled
}

#output "eDP-1" disable
#output "HDMI-A-1" resolution 2560x1440
output "*" background ~/.cache/wallpaper fill

set $laptop eDP-1
bindswitch --reload --locked lid:on output $laptop disable
bindswitch --reload --locked lid:off output $laptop enable

xwayland enable
